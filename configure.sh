#!/bin/bash

docker network create xpra-network --subnet=10.0.0.0/24
docker build -t xpra-ubuntu - < Dockerfile
docker build -t xpra-xterm - < Dockerfile.xterm
