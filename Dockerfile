FROM ubuntu:jammy
VOLUME /tmp/.X11-unix
RUN apt update \
 && DEBIAN_FRONTEND=noninteractive apt install -y wget gnupg xvfb x11-xserver-utils python3-pip \
 && pip3 install pyinotify \
 && echo "deb [arch=amd64] https://xpra.org/ jammy main" > /etc/apt/sources.list.d/xpra.list \
 && wget -q https://xpra.org/gpg.asc -O- | apt-key add - \
 && apt update \
 && DEBIAN_FRONTEND=noninteractive apt install -y xpra \
 && mkdir -p /run/user/0/xpra
RUN apt install -y xterm iproute2 
# RUN XDG_MENU_PREFIX=gnome- python3 -c "from xdg.Menu import parse;parse()"
ENTRYPOINT ["xpra", "start", ":80", "--bind-tcp=0.0.0.0:8080", \
 "--mdns=no", "--webcam=no", "--no-daemon", \
 "--start=xhost +"]
