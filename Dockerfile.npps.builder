FROM ubuntu:kinetic
RUN apt update
# RUN apt install -y protobuf*
RUN apt install -y iproute2 sudo cmake make g++ libcrypto++* qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools
RUN useradd -rm -u 1001 usr -G sudo -g root -s /bin/bash \
 && sed -i '/%sudo.*ALL/a %sudo ALL=(ALL) NOPASSWD: ALL' /etc/sudoers
USER usr
WORKDIR /build
